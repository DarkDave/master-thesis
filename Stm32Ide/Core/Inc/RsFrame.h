/*
 * RsFrame.h
 *
 *  Created on: Apr 16, 2021
 *      Author: dave
 */

#ifndef INC_RSFRAME_H_
#define INC_RSFRAME_H_

#include "stdint.h"

#define DATA_FRAME_SIZE 256

typedef struct __attribute__((packed))
{
	uint8_t srcAddress;		// Link layer address of sending device.
	uint8_t dstAddress;		// Link layer address of recipient device.
	uint16_t size;				// Number of bytes with data contained in a frame.
	uint8_t data[DATA_FRAME_SIZE];
	uint32_t crc;					// CRC calculated from all above fields.
} RsFrame;

const uint32_t RsFrameSizeWithoutCrc = (sizeof(RsFrame) - sizeof(uint32_t)) / sizeof(uint32_t);

const uint8_t BROADCAST_ADDRESS = 0xffu;

//void RsFrameCopy(RsFrame *pDstFrame, const RsFrame *pSrcFrame)
//{
//	pDstFrame->frameType = pSrcFrame->frameType;
//	pDstFrame->size = pSrcFrame->size;
//	memcpy(pDstFrame->data, pSrcFrame->data, pDstFrame->size);
//}
//
//RsFrame RsFrameCreate(enum FrameType frameType, const uint8_t *pData, size_t size)
//{
//	RsFrame frame = {.frameType = frameType, .size = size};
//	memcpy(frame.data, pData, size);
//
//	return frame;
//}

#endif /* INC_RSFRAME_H_ */
