/*
 * Pin.h
 *
 *  Created on: Jan 6, 2021
 *      Author: dave
 */

#ifndef INC_PIN_H_
#define INC_PIN_H_

#include "stm32l1xx_hal.h"

typedef struct
{
	GPIO_TypeDef* port;
	uint16_t pin;
} Pin;

void setPinState(const Pin p, GPIO_PinState state);

void setPin(const Pin p);
void resetPin(const Pin p);


#endif /* INC_PIN_H_ */
