#ifndef CONTROLLER_DEFINITION
#define CONTROLLER_DEFINITION

#define GREEN_CONTROLLER 0
#define BLUE_CONTROLLER 1

#define RECEIVER_MODE 0
#define TRANSCEIVER_MODE 1

#ifdef CONTROLLER_TYPE
#error "Only one controller can be active at a time."
#else
#define CONTROLLER_TYPE GREEN_CONTROLLER
#define UART_MODE RECEIVER_MODE
#endif


const uint8_t MODULE_ADDRESS = 2u;
const uint8_t RECIPIENT_ADDRESS = 1u;


#endif
