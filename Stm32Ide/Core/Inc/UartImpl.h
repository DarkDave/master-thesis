/*
 * UartImpl.h
 *
 *  Created on: Jan 6, 2021
 *      Author: dave
 */

#ifndef INC_UARTIMPL_H_
#define INC_UARTIMPL_H_

#include <stdbool.h>

#include "main.h"
#include "Pin.h"


typedef struct
{
	UART_HandleTypeDef *pUart;
	Pin enablePin;
	Pin ledPin;
	volatile bool *pUartToken;
} UartHandle;

typedef enum
{
  UART_MODE_BLOCKING,
	UART_MODE_IT,
	UART_MODE_DMA
} UartMode;
//
//extern const Pin LeftUartEnableTxPin;
//extern const Pin RightUartEnableTxPin;
//
//extern const Pin LeftLedPin;
//extern const Pin RightLedPin;

extern UartHandle LeftUart;
extern UartHandle RightUart;

//extern volatile bool LeftUartToken;
//extern volatile bool RightUartToken;

void initUartHandles(UART_HandleTypeDef *leftHuart, UART_HandleTypeDef *rightHuart);
void initUartPins();

void acquireUartToken(const UartHandle uart);
void relinquishUartToken(const UartHandle uart);

bool getToken(const UartHandle uart);

#endif /* INC_UARTIMPL_H_ */
