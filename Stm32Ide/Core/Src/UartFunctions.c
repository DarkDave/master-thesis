/*
 * UartFunctions.c
 *
 *  Created on: Apr 14, 2021
 *      Author: dave
 */
// Returns true if calculated CRC from the frame equals received one.
//bool IsDataFrameValid(const RsFrame *pFrame)
//{
//	// TODO implementation needed
//	(void)pFrame;
//	return 1;
//}
//
//// Returns true if similar frame doesn't exist in last n received data frames.
//// Frames are similar if they were sent from the same address and have the same Flow Label.
//bool IsDataFrameUnique(const RsFrame *pFrame)
//{
//	// TODO implementation needed
//	(void)pFrame;
//	return 1;
//}
//
//void ProcessDataFrame(const RsFrame *pFrame)
//{
//	// TODO implementation needed
//	(void)pFrame;
//}

//RsFrame LeftUartTransmitFrame; // Currently sent frame.
//void LeftUartTransmit()
//{
//	// Try to get frame from the transmit queue.
//	if (QueuePeek(&LeftTransmitQueue, &LeftUartTransmitFrame))
//	{
//		// If there are no frames, empty frame must be sent.
//		LeftUartTransmitFrame.frameType = FRAME_TYPE_DATA;
//		LeftUartTransmitFrame.size = DATA_FRAME_SIZE;
//		memcpy(LeftUartTransmitFrame.data, EmptyFrame, DATA_FRAME_SIZE);
//	}
//
//	LeftUartLastSentFrameType = LeftUartTransmitFrame.frameType;
//
//	setPin(LeftUart.enablePin);
//
//	UartTransmit(LeftUart.pUart, &LeftUartTransmitFrame);
//}
//
//RsFrame RightUartTransmitFrame;
//void RightUartTransmit()
//{
//	// Try to get frame from the transmit queue.
//	if (QueuePeek(&RightTransmitQueue, &RightUartTransmitFrame))
//	{
//		// If there are no frames, empty frame must be sent.
//		RightUartTransmitFrame.frameType = FRAME_TYPE_DATA;
//		RightUartTransmitFrame.size = DATA_FRAME_SIZE;
//		memcpy(RightUartTransmitFrame.data, EmptyFrame, DATA_FRAME_SIZE);
//	}
//
//	RightUartLastSentFrameType = RightUartTransmitFrame.frameType;
//
//	setPin(RightUart.enablePin);
//
//	UartTransmit(RightUart.pUart, &RightUartTransmitFrame);
//}
//
//RsFrame LeftUartReceiveFrame;
//void LeftUartReceiveConfirmation()
//{
//	LEFT_UART_RECEIVE_TIMER.Instance->CNT = 0;
//	HAL_TIM_Base_Start_IT(&LEFT_UART_RECEIVE_TIMER);
//
//	LeftUartReceiveFrame.frameType = FRAME_TYPE_CONFIRMATION;
//	LeftUartReceiveFrame.size = CONFIRMATION_FRAME_SIZE;
//	HAL_UART_Receive_IT(LeftUart.pUart, LeftUartReceiveFrame.data, LeftUartReceiveFrame.size);
//}
//
//void LeftUartReceiveDataFrame()
//{
//	LEFT_UART_RECEIVE_TIMER.Instance->CNT = 0;
//	HAL_TIM_Base_Start_IT(&LEFT_UART_RECEIVE_TIMER);
//
//	LeftUartReceiveFrame.frameType = FRAME_TYPE_DATA;
//	LeftUartReceiveFrame.size = DATA_FRAME_SIZE;
//	HAL_UART_Receive_IT(LeftUart.pUart, LeftUartReceiveFrame.data, LeftUartReceiveFrame.size);
//}
//
//RsFrame RightUartReceiveFrame;
//void RightUartReceiveConfirmation()
//{
//	RIGHT_UART_RECEIVE_TIMER.Instance->CNT = 0;
//	HAL_TIM_Base_Start_IT(&RIGHT_UART_RECEIVE_TIMER);
//
//	RightUartReceiveFrame.frameType = FRAME_TYPE_CONFIRMATION;
//	RightUartReceiveFrame.size = CONFIRMATION_FRAME_SIZE;
//	HAL_UART_Receive_IT(RightUart.pUart, RightUartReceiveFrame.data, RightUartReceiveFrame.size);
//}
//
//void RightUartReceiveDataFrame()
//{
//	RIGHT_UART_RECEIVE_TIMER.Instance->CNT = 0;
//	HAL_TIM_Base_Start_IT(&RIGHT_UART_RECEIVE_TIMER);
//
//	RightUartReceiveFrame.frameType = FRAME_TYPE_DATA;
//	RightUartReceiveFrame.size = DATA_FRAME_SIZE;
//	HAL_UART_Receive_IT(RightUart.pUart, RightUartReceiveFrame.data, RightUartReceiveFrame.size);
//}
//
//void UartTransmitRsFrame(UART_HandleTypeDef *huart, RsFrame *pFrame)
//{
//	HAL_UART_Transmit_IT(huart, (uint8_t*)pFrame, sizeof(RsFrame));
//}
//
//void UartTransmit(UART_HandleTypeDef *huart, RsFrame *pFrame)
//{
//	HAL_UART_Transmit_IT(huart, pFrame->data, pFrame->size);
//}
//
//void LeftUartTransmitComplete()
//{
//	resetPin(LeftUart.enablePin);
//	if (LeftUartLastSentFrameType == FRAME_TYPE_DATA)
//	{
//		LeftUartReceiveConfirmation();
//	} else if (LeftUartLastSentFrameType == FRAME_TYPE_CONFIRMATION)
//	{
//		QueuePop(&LeftTransmitQueue);
//
//		// Check if last frame was a request for resend or confirmation of succesful reception.
//		if (memcmp(LeftUartTransmitFrame.data, ConfirmationSuccessfulFrame, CONFIRMATION_FRAME_SIZE) == 0)
//		{
//			LeftUartTransmit();
//		} else
//		{
//			LeftUartReceiveDataFrame();
//		}
//	} else
//	{
//		// FRAME_TYPE_NO_FRAME - on left UART start receiving data frame.
//		LeftUartReceiveDataFrame();
//	}
//}
//
//void RightUartTransmitComplete()
//{
//	resetPin(RightUart.enablePin);
//	if (RightUartLastSentFrameType == FRAME_TYPE_DATA)
//	{
//		RightUartReceiveConfirmation();
//	} else if (RightUartLastSentFrameType == FRAME_TYPE_CONFIRMATION)
//	{
//		QueuePop(&RightTransmitQueue);
//
//		// Check if last frame was a request for resend or confirmation of succesful reception.
//		if (memcmp(RightUartTransmitFrame.data, ConfirmationSuccessfulFrame, CONFIRMATION_FRAME_SIZE) == 0)
//		{
//			RightUartTransmit();
//		} else
//		{
//			RightUartReceiveDataFrame();
//		}
//	} else
//	{
//		// FRAME_TYPE_NO_FRAME - on right UART start transmitting data frame.
//		RightUartTransmit();
//	}
//}
//
//void LeftUartReceiveConfirmationComplete()
//{
//	if (memcmp(LeftUartReceiveFrame.data, ConfirmationSuccessfulFrame, CONFIRMATION_FRAME_SIZE) == 0)
//	{ // Last sent frame was received successfully.
//		// TODO consider checking if this is really the frame that was sent.
//		QueuePop(&LeftTransmitQueue);
//
//		LeftUartReceiveDataFrame();
//	}
//	else
//	{ // Recipient asked for data frame retransmit.
//		LeftUartTransmit();
//	}
//}
//
//void LeftUartReceiveDataFrameComplete()
//{
//	const RsFrame *pReceivedFrame = &LeftUartReceiveFrame;
//	RsFrame confirmationFrame;
//
//	if (IsDataFrameValid(pReceivedFrame))
//	{
//		if(IsDataFrameUnique(pReceivedFrame))
//		{
//			ProcessDataFrame(pReceivedFrame);
//		}
//		confirmationFrame = RsFrameCreate(FRAME_TYPE_CONFIRMATION, ConfirmationSuccessfulFrame, CONFIRMATION_FRAME_SIZE);
//	}
//	else
//	{
//		confirmationFrame = RsFrameCreate(FRAME_TYPE_CONFIRMATION, RequestResendFrame, CONFIRMATION_FRAME_SIZE);
//	}
//	QueuePushFront(&LeftTransmitQueue, &confirmationFrame);
//
//	LeftUartTransmit();
//}
//
//void RightUartReceiveConfirmationComplete()
//{
//	if (memcmp(RightUartReceiveFrame.data, ConfirmationSuccessfulFrame, CONFIRMATION_FRAME_SIZE) == 0)
//	{ // Last sent frame was received successfully.
//		// TODO consider checking if this is really the frame that was sent.
//		QueuePop(&RightTransmitQueue);
//
//		RightUartReceiveDataFrame();
//	}
//	else
//	{ // Recipient asked for data frame retransmit.
//		RightUartTransmit();
//	}
//}
//
//void RightUartReceiveDataFrameComplete()
//{
//	const RsFrame *pReceivedFrame = &RightUartReceiveFrame;
//	RsFrame confirmationFrame;
//
//	if (IsDataFrameValid(pReceivedFrame))
//	{
//		if(IsDataFrameUnique(pReceivedFrame))
//		{
//			ProcessDataFrame(pReceivedFrame);
//		}
//		confirmationFrame = RsFrameCreate(FRAME_TYPE_CONFIRMATION, ConfirmationSuccessfulFrame, CONFIRMATION_FRAME_SIZE);
//	}
//	else
//	{
//		confirmationFrame = RsFrameCreate(FRAME_TYPE_CONFIRMATION, RequestResendFrame, CONFIRMATION_FRAME_SIZE);
//	}
//	QueuePushFront(&RightTransmitQueue, &confirmationFrame);
//
//	RightUartTransmit();
//}
