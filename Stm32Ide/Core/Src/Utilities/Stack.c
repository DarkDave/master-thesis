/*
 * Stack.c
 *
 *  Created on: Apr 17, 2021
 *      Author: dave
 */

#include "stdlib.h"

#include "Stack.h"


bool StackInit(Stack* pStack, size_t size)
{
	pStack->size = size;
	pStack->top = -1;
	return (NULL == (pStack->pEntries = calloc(size, sizeof(size_t))));
}

void StackDeInit(Stack* pStack)
{
	free(pStack->pEntries);
	pStack->pEntries = NULL;
	pStack->size = 0;
}

bool StackIsEmpty(Stack* pStack)
{
	return (pStack->top == -1);
}

bool StackIsFull(Stack* pStack)
{
	return (pStack->top == (pStack->size - 1));
}

void StackPush(Stack* pStack, size_t newElement)
{
	pStack->pEntries[++pStack->top] = newElement;
}

size_t StackPop(Stack* pStack)
{
	return pStack->pEntries[pStack->top--];
}
