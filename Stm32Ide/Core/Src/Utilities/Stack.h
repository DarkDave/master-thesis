/*
 * Stack.h
 *
 *  Created on: Apr 17, 2021
 *      Author: dave
 */

#ifndef SRC_UTILITIES_STACK_H_
#define SRC_UTILITIES_STACK_H_


#include "stddef.h"
#include "stdbool.h"


typedef struct
{
	int top;				  // Current top index.
	size_t size;			// Maximum number of elements.
	size_t* pEntries;
} Stack;


// Allocates memory for a maximum of size stack entries.
// Returns 0 if stack was created successfully.
bool StackInit(Stack* pStack, size_t size);

// Frees allocated memory.
void StackDeInit(Stack* pStack);

// Returns true if no entries have been added on the stack.
bool StackIsEmpty(Stack* pStack);

// Returns true if no more entries can be placed on the stack.
bool StackIsFull(Stack* pStack);

// Adds newElement on the top of the stack.
// Behavior is undefined if stack is full.
void StackPush(Stack* pStack, size_t newElement);
// TODO should this be safe or it can stay like this?

// Returns top element and removes it from the stack.
// Behavior is undefined if stack is empty.
size_t StackPop(Stack* pStack);


#endif /* SRC_UTILITIES_STACK_H_ */
