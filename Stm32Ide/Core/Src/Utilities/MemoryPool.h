/*
 * MemoryPool.h
 *
 *  Created on: Apr 17, 2021
 *      Author: dave
 */

#ifndef SRC_UTILITIES_MEMORYPOOL_H_
#define SRC_UTILITIES_MEMORYPOOL_H_


#include "stdint.h"
#include "stddef.h"
#include "stdbool.h"

#include "Stack.h"


typedef struct MemoryPool_t MemoryPool;

// Structure for holding a single memory block.
typedef struct
{
	void* pData; 							// Pointer to allocated memory.
	size_t index; 						// Index used for tracking free and taken memory blocks.
	size_t useCount; 					// Number of users of this block.
														// Should be increased if block will be passed to more than 1 user.
	MemoryPool* pPool; // Pointer to Memory pool which this block belongs to.
} MemoryBlock;

typedef struct MemoryPool_t
{
	size_t nBlocks; 				// Number of blocks available in created MemoryPool.
	size_t blockSize; 			// Memory of a single block.
	Stack freeBlocksStack;	// Stack for keeping indexes of free blocks.
	MemoryBlock* pBlocks; 	// Array with allocated memory blocks.
} MemoryPool;


// Allocates memory for nBlocks elements of size blockSize.
// Returns 0 if MemoryPool was created successfully.
bool MemoryPoolInit(MemoryPool* pMemoryPool, size_t nBlocks, size_t blockSize);

// Deallocates memory in passed MemoryPool.
void MemoryPoolDeInit(MemoryPool* pMemoryPool);

// Returns pointer to a free memory block or NULL if no memory blocks are available.
MemoryBlock* MemoryPoolGetBlock(MemoryPool* pMemoryPool);

// Releases MemoryBlock obtained by MemoryPoolGetBlock to the MemoryPool.
// Return 0 if block was successfully released.
bool MemoryPoolReleaseBlock(MemoryPool* pMemoryPool, MemoryBlock* pBlock);


#endif /* SRC_UTILITIES_MEMORYPOOL_H_ */
