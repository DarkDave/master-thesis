/*
 * MemoryPool.c
 *
 *  Created on: Apr 17, 2021
 *      Author: dave
 */

#include "stdlib.h"

#include "MemoryPool.h"


// Deallocates first allocatedElements blocks in memory pool as well as the pMemoryPool's block array.
static void MemoryPoolPartialDeInit(MemoryPool* pMemoryPool, size_t allocatedElements)
{
	for (size_t i = 0; i < allocatedElements; ++i)
	{
		free(pMemoryPool->pBlocks[i].pData);
		pMemoryPool->pBlocks[i].pData = NULL;
	}

	free(pMemoryPool->pBlocks);
	pMemoryPool->pBlocks = NULL;

	StackDeInit(&pMemoryPool->freeBlocksStack);
}

bool MemoryPoolInit(MemoryPool* pMemoryPool, size_t nBlocks, size_t blockSize)
{
	pMemoryPool->nBlocks = nBlocks;
	if (StackInit(&pMemoryPool->freeBlocksStack, pMemoryPool->nBlocks))
	{
		return 1;
	}

	if (NULL == (pMemoryPool->pBlocks = (MemoryBlock*)calloc(nBlocks, sizeof(MemoryBlock))))
	{
		StackDeInit(&pMemoryPool->freeBlocksStack);
		return 1;
	}

	pMemoryPool->blockSize = blockSize;
	for (size_t i = 0; i < nBlocks; ++i)
	{
		pMemoryPool->pBlocks[i].index = i;
		pMemoryPool->pBlocks[i].useCount = 0;
		pMemoryPool->pBlocks[i].pPool = pMemoryPool;
		StackPush(&pMemoryPool->freeBlocksStack, i);
		if (NULL == (pMemoryPool->pBlocks[i].pData = malloc(blockSize)))
		{
			// If allocation failed, free already allocated memory and return 0.
			MemoryPoolPartialDeInit(pMemoryPool, i);
			return 1;
		}
	}

	return 0;
}

void MemoryPoolDeInit(MemoryPool* pMemoryPool)
{
	MemoryPoolPartialDeInit(pMemoryPool, pMemoryPool->nBlocks);
}

MemoryBlock* MemoryPoolGetBlock(MemoryPool* pMemoryPool)
{
	if (StackIsEmpty(&pMemoryPool->freeBlocksStack))
	{
		return NULL;
	}
	else
	{
		const size_t freeBlockIndex = StackPop(&pMemoryPool->freeBlocksStack);
		pMemoryPool->pBlocks[freeBlockIndex].useCount = 1;
		return pMemoryPool->pBlocks + freeBlockIndex;
	}
}

bool MemoryPoolReleaseBlock(MemoryPool* pMemoryPool, MemoryBlock* pBlock)
{
	if (StackIsFull(&pMemoryPool->freeBlocksStack))
	{
		return 1;
	}
	else
	{
		if (--pBlock->useCount == 0)
		{
			StackPush(&pMemoryPool->freeBlocksStack, pBlock->index);
		}
		return 0;
	}
}
