/*
 * PtrRingBuffer.c
 *
 *  Created on: Apr 16, 2021
 *      Author: dave
 */

#include "RingBuffer.h"


bool RingBufferInit(RingBuffer *pRingBuffer, size_t maxElems)
{
	pRingBuffer->write = 0U;
	pRingBuffer->read = 0U;
	pRingBuffer->maxElems = maxElems;
	pRingBuffer->pData = calloc(maxElems + 1, sizeof(void*));

	return NULL == pRingBuffer->pData;
}

void RingBufferDeinit(RingBuffer *pRingBuffer)
{
	pRingBuffer->maxElems = 0U;
	free(pRingBuffer->pData);
	pRingBuffer->pData = NULL;
}

bool RingBufferIsEmpty(RingBuffer* pRingBuffer)
{
	return (pRingBuffer->read == pRingBuffer->write);
}

bool RingBufferIsFull(RingBuffer* pRingBuffer)
{
	return (pRingBuffer->write + 1 == pRingBuffer->read)
			|| ((pRingBuffer->read == 0) && (pRingBuffer->write == pRingBuffer->maxElems));
}

// TODO should this be overwriting oldest entries? What about releasing the memory to memory pool.
void RingBufferPut(RingBuffer* pRingBuffer, void* pNewEntry)
{
	pRingBuffer->pData[pRingBuffer->write] = pNewEntry;
	pRingBuffer->write = (pRingBuffer->write == pRingBuffer->maxElems) ? 0U : pRingBuffer->write + 1;

	if (pRingBuffer->write == pRingBuffer->read) // If buffer was full, remove the oldest entry.
	{
		pRingBuffer->read = (pRingBuffer->read == pRingBuffer->maxElems) ? 0U : pRingBuffer->read + 1;
	}
}

void* RingBufferGet(RingBuffer* pRingBuffer)
{
	if (RingBufferIsEmpty(pRingBuffer)) // Ring buffer is empty.
	{
		return NULL;
	}
	else
	{
		void* pEntry = pRingBuffer->pData[pRingBuffer->read];
		pRingBuffer->read = (pRingBuffer->read == pRingBuffer->maxElems) ? 0U : pRingBuffer->read + 1;
		return pEntry;
	}
}
