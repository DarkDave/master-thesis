/*
 * PtrRingBuffer.h
 *
 *  Created on: Apr 16, 2021
 *      Author: dave
 */

#ifndef SRC_UTILITIES_RINGBUFFER_H_
#define SRC_UTILITIES_RINGBUFFER_H_

#include "stdlib.h"
#include "stdbool.h"


typedef struct
{
	size_t write;
	size_t read;
	size_t maxElems;
	void** pData;
} RingBuffer;

// Initialize ring buffer to be able to hold maxElems void pointers.
// Returns 0 if ring buffer was created successfully.
bool RingBufferInit(RingBuffer* pRingBuffer, size_t maxElems);

// Deinitialize ring buffer.
void RingBufferDeinit(RingBuffer* pRingBuffer);

// Returns true if buffer is empty.
bool RingBufferIsEmpty(RingBuffer* pRingBuffer);

// Returns true if buffer is full.
bool RingBufferIsFull(RingBuffer* pRingBuffer);

// Put pNewEntry to the buffer.
// If there's no more space, overwrite oldest entry.
void RingBufferPut(RingBuffer* pRingBuffer, void* pNewEntry);

// Get oldest entry.
// Returns NULL if buffer is empty.
void* RingBufferGet(RingBuffer* pRingBuffer);


#endif /* SRC_UTILITIES_RINGBUFFER_H_ */
