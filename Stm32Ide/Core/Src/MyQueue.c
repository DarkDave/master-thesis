//// Initialize queue to be able to hold queueSize - 1 frames.
//void QueueInit(Queue *queue, size_t queueSize)
//{
//	queue->write = 0;
//	queue->read = 0;
//	queue->size = queueSize;
//	queue->pFrames = calloc(queueSize, sizeof(RsFrame));
//}
//
//// Deinitialize queue.
//void QueueDeinit(Queue *pQueue)
//{
//	pQueue->size = 0;
//	free(pQueue->pFrames);
//	pQueue->pFrames = NULL;
//}
//
//// Return 0 if frame was successfully obtained from given queue or 1 if queue is empty.
//// Obtained frame is copied to the given frame pointer.
//int QueuePeek(Queue *pQueue, RsFrame *pFrameOut)
//{
//	if (pQueue->write == pQueue->read) // Queue is empty.
//	{
//		return 1;
//	}
//	RsFrameCopy(pFrameOut, &pQueue->pFrames[pQueue->read]);
//	return 0;
//}
//
//// Discard oldest queue entry.
//void QueuePop(Queue *pQueue)
//{
//	if (pQueue->write != pQueue->read) // Remove only when queue is not empty.
//	{
//		pQueue->read = (pQueue->read + 1) % pQueue->size;
//	}
//}
//
//// Add new entry to the queue. If there is no more space, function deletes the oldest entry.
//void QueuePushBack(Queue *pQueue, const RsFrame *pFrame)
//{
//	RsFrameCopy(&pQueue->pFrames[pQueue->write], pFrame);
//	pQueue->write = (pQueue->write + 1) % pQueue->size;
//
//	if (pQueue->write == pQueue->read) // If queue was full, remove the oldest entry.
//	{
//		pQueue->read = (pQueue->read + 1) % pQueue->size; // Discard oldest entry.
//	}
//
//}
//
//// Add frame as the oldest one, so the new frame will be read first.
//// If there is no more space, overwrite oldest queue entry.
//void QueuePushFront(Queue *pQueue, const RsFrame *pFrame)
//{
//	if (pQueue->write == pQueue->read)
//	{
//		QueuePushBack(pQueue, pFrame);
//	} else
//	{
//	  if (((pQueue->write + 1) % pQueue->size) != pQueue->read)
//	  { // If adding one more frame makes the queue full, oldest frame can be overwritten and read index remains unchanged.
//	  	// Otherwise new frame must be put one position before current oldest frame (read).
//	  	pQueue->read = (pQueue->read + pQueue->size - 1) % pQueue->size;
//	  }
//  	RsFrameCopy(&pQueue->pFrames[pQueue->read], pFrame);
//	}
//}
