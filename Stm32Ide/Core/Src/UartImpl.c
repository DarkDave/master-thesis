/*
 * UartImpl.c
 *
 *  Created on: Jan 6, 2021
 *      Author: dave
 */

#include "UartImpl.h"


volatile bool LeftUartToken = 0;
volatile bool RightUartToken = 1;

const Pin LeftUartEnableTxPin = {USART3_TX_ENABLE_GPIO_Port, USART3_TX_ENABLE_Pin};
const Pin RightUartEnableTxPin = {USART1_TX_ENABLE_GPIO_Port, USART1_TX_ENABLE_Pin};

const Pin LeftLedPin = {LD3_GPIO_Port, LD3_Pin};
const Pin RightLedPin = {LD4_GPIO_Port, LD4_Pin};

UartHandle LeftUart;
UartHandle RightUart;

void initUartHandles(UART_HandleTypeDef *leftHuart, UART_HandleTypeDef *rightHuart)
{
  LeftUart.pUart = leftHuart;
	LeftUart.enablePin = LeftUartEnableTxPin;
	LeftUart.ledPin = LeftLedPin;
	LeftUart.pUartToken = &LeftUartToken;

  RightUart.pUart = rightHuart;
  RightUart.enablePin = RightUartEnableTxPin;
  RightUart.ledPin = RightLedPin;
  RightUart.pUartToken = &RightUartToken;
}

void initUartPins()
{
	resetPin(LeftUart.enablePin);
	resetPin(RightUart.enablePin);

//	setPinState(LeftUart.ledPin, getToken(LeftUart));
//	setPinState(RightUart.ledPin, getToken(RightUart));
}

void acquireUartToken(const UartHandle uart)
{
	*uart.pUartToken = 1;
	setPin(uart.ledPin);
}

void relinquishUartToken(const UartHandle uart)
{
	*uart.pUartToken = 0;
	resetPin(uart.ledPin);
}

bool getToken(const UartHandle uart)
{
	return *uart.pUartToken;
}
