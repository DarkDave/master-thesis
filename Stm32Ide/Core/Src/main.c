/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
#include <stdlib.h>

#include "ControllerDefinition.h"
#include "UartImpl.h"
#include "Pin.h"
#include "RsFrame.h"
#include "Utilities/MemoryPool.h"
#include "Utilities/RingBuffer.h"
#include "Utilities/Stack.h"

//#define RINGBUFFER_AVOID_MODULO 1
//#include "../../ThirdParty/c-generic-ring-buffer/ringbuffer.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define RIGHT_UART_INSTANCE USART1
#define LEFT_UART_INSTANCE USART2

#define DONT_USE_MIC_BUTTON 1

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc;
DMA_HandleTypeDef hdma_adc;

CRC_HandleTypeDef hcrc;

DAC_HandleTypeDef hdac;
DMA_HandleTypeDef hdma_dac_ch2;

TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim7;
TIM_HandleTypeDef htim10;
TIM_HandleTypeDef htim11;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart1_rx;
DMA_HandleTypeDef hdma_usart1_tx;
DMA_HandleTypeDef hdma_usart2_rx;
DMA_HandleTypeDef hdma_usart2_tx;

/* USER CODE BEGIN PV */

// ADC Vref = VDD = 2.95V.

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_TIM10_Init(void);
static void MX_TIM11_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_ADC_Init(void);
static void MX_CRC_Init(void);
static void MX_DAC_Init(void);
static void MX_TIM6_Init(void);
static void MX_TIM7_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */

static inline bool IsLeftUart(UART_HandleTypeDef *huart)
{
	return huart->Instance == LEFT_UART_INSTANCE;
}
static inline bool IsRightUart(UART_HandleTypeDef *huart)
{
	return huart->Instance == RIGHT_UART_INSTANCE;
}

static inline bool IsMicrophoneButtonPressed()
{
	return HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin) == GPIO_PIN_SET;
}

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

MemoryPool FramesMemoryPool;
MemoryPool RxFramesMemoryPool;
RingBuffer PlayingBuffer;
RingBuffer TxBuffer;


// OS definitions

typedef struct
{
	void (*pTask)(void*);
	volatile bool ready;
} Task;

#define MAX_TASKS 4

volatile Task Tasks[MAX_TASKS];
void* TasksArguments[MAX_TASKS];

#define DAC_TASK_NUMBER 0
#define ADC_TASK_NUMBER 1
#define LEFT_UART_TASK_NUMBER 2
#define RIGHT_UART_TASK_NUMBER 3


const size_t ADC_TRANSFER_SIZE = DATA_FRAME_SIZE / 2;
MemoryBlock* pAdcOldFrame = NULL;
MemoryBlock* pAdcNewFrame = NULL;

// Task tries to get memory block for storing new audio data.
// Upon getting block, task starts ADC in DMA mode.
void AdcTask(void* pArg)
{
	if (DONT_USE_MIC_BUTTON || IsMicrophoneButtonPressed())
	{
		__disable_irq();
		pAdcNewFrame = MemoryPoolGetBlock(&FramesMemoryPool);
		__enable_irq();

		if (NULL != pAdcNewFrame)
		{
			HAL_ADC_Start_DMA(&hadc, (uint32_t*)(((RsFrame*)pAdcNewFrame->pData)->data), ADC_TRANSFER_SIZE);
			HAL_TIM_Base_Start(&htim6);
			Tasks[ADC_TASK_NUMBER].ready = 0;
		}
	}
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	pAdcOldFrame = pAdcNewFrame;

	if (DONT_USE_MIC_BUTTON || IsMicrophoneButtonPressed())
	{
		pAdcNewFrame = MemoryPoolGetBlock(&FramesMemoryPool);
		if (NULL != pAdcNewFrame)
		{
			HAL_ADC_Start_DMA(hadc, (uint32_t*)(((RsFrame*)pAdcNewFrame->pData)->data), ADC_TRANSFER_SIZE);
		}
		else
		{
			HAL_TIM_Base_Stop(&htim6);
			Tasks[ADC_TASK_NUMBER].ready = 1;
		}

		if (RingBufferIsFull(&TxBuffer))
		{
			MemoryPoolReleaseBlock(pAdcOldFrame->pPool, pAdcOldFrame); // If there is no more space in transmit buffer, discard newly acquired frame.
		}
		else
		{
			RingBufferPut(&TxBuffer, (void*)pAdcOldFrame);
		}

		Tasks[RIGHT_UART_TASK_NUMBER].ready = 1;
	}
	else
	{
			HAL_TIM_Base_Stop(&htim6);
			Tasks[ADC_TASK_NUMBER].ready = 1;
			MemoryPoolReleaseBlock(pAdcOldFrame->pPool, pAdcOldFrame); // If there is no more space in transmit buffer, discard newly acquired frame.
	}

}


const size_t DAC_TRANSFER_SIZE = DATA_FRAME_SIZE / 2;
MemoryBlock* pDacOldFrame = NULL;
MemoryBlock* pDacNewFrame = NULL;
volatile bool DacTransferActive = 0;

void DacTask(void* pArg)
{
	if (DacTransferActive)
		return;

	__disable_irq();
	pDacNewFrame = (MemoryBlock*)RingBufferGet(&PlayingBuffer);
	__enable_irq();

	if (NULL != pDacNewFrame)
	{
		HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_2, (uint32_t*)(((RsFrame*)pDacNewFrame->pData)->data), DAC_TRANSFER_SIZE, DAC_ALIGN_12B_R);
		HAL_TIM_Base_Start_IT(&htim7);
		DacTransferActive = 1;
		Tasks[DAC_TASK_NUMBER].ready = 0;
	}
}

void HAL_DACEx_ConvCpltCallbackCh2(DAC_HandleTypeDef* hdac)
{
	pDacOldFrame = pDacNewFrame;

	pDacNewFrame = (MemoryBlock*)RingBufferGet(&PlayingBuffer);
	if (NULL != pDacNewFrame)
	{
		const HAL_StatusTypeDef status = HAL_DAC_Start_DMA(hdac, DAC_CHANNEL_2, (uint32_t*)(((RsFrame*)pDacNewFrame->pData)->data), DAC_TRANSFER_SIZE, DAC_ALIGN_12B_R);
		if (status != HAL_OK)
			Error_Handler();
	}
	else
	{
		HAL_TIM_Base_Stop(&htim7);
		DacTransferActive = 0;
		Tasks[DAC_TASK_NUMBER].ready = 1;
	}

	MemoryPoolReleaseBlock(pDacOldFrame->pPool, pDacOldFrame);
}


MemoryBlock* pTxBlock = NULL;
volatile bool TransmitInProgress = 0;

void TransmitterTask(void* pArg)
{
	if (TransmitInProgress)
		return;

	__disable_irq();
	pTxBlock = RingBufferGet(&TxBuffer);
	__enable_irq();
	if (NULL == pTxBlock)
		return;

	// Add header to frame.
	((RsFrame*)pTxBlock->pData)->srcAddress = MODULE_ADDRESS;
	((RsFrame*)pTxBlock->pData)->dstAddress = RECIPIENT_ADDRESS;
	((RsFrame*)pTxBlock->pData)->size = DATA_FRAME_SIZE;

	// CRC calculation.
	((RsFrame*)pTxBlock->pData)->crc = HAL_CRC_Calculate(&hcrc, (uint32_t*)pTxBlock->pData, RsFrameSizeWithoutCrc);

	setPin(RightUart.enablePin);
	HAL_UART_Transmit_DMA(RightUart.pUart, (uint8_t*)pTxBlock->pData, sizeof(RsFrame));
	TransmitInProgress = 1;
	Tasks[RIGHT_UART_TASK_NUMBER].ready = 0;
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	resetPin(RightUart.enablePin);
	TransmitInProgress = 0;
	MemoryPoolReleaseBlock(pTxBlock->pPool, pTxBlock);
}

// TODO this might be deleted later.
volatile uint32_t InvalidCrc = 0;
volatile uint32_t ValidCrc = 0;

MemoryBlock* pRxBlock = NULL;
volatile bool FrameReceived = 0;

void ReceiverTask(void* pArg)
{
	if (FrameReceived)
	{
		FrameReceived = 0;

		// CRC validation.
		const uint32_t crc = HAL_CRC_Calculate(&hcrc, (uint32_t*)pRxBlock->pData, RsFrameSizeWithoutCrc);
		if (crc == ((RsFrame*)pRxBlock->pData)->crc)
		{
			++ValidCrc;
			HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin);

			if (((RsFrame*)pRxBlock->pData)->srcAddress == MODULE_ADDRESS)
			{
				// Frame came back to originating module - discard it.
				__disable_irq();
				MemoryPoolReleaseBlock(pRxBlock->pPool, pRxBlock);
				__enable_irq();
			}
			else
			{
				const bool shouldBePlayed = ( ((RsFrame*)pRxBlock->pData)->dstAddress == MODULE_ADDRESS
																			|| ((RsFrame*)pRxBlock->pData)->dstAddress == BROADCAST_ADDRESS );
				const bool shouldBeTransmitted = ( ((RsFrame*)pRxBlock->pData)->dstAddress != MODULE_ADDRESS
																					 || ((RsFrame*)pRxBlock->pData)->dstAddress == BROADCAST_ADDRESS );

				if (shouldBePlayed && shouldBeTransmitted)
				{
					++pRxBlock->useCount;
				}

				if (shouldBePlayed)
				{
					__disable_irq();
					if (RingBufferIsFull(&PlayingBuffer))
					{
						MemoryPoolReleaseBlock(pRxBlock->pPool, pRxBlock);
					}
					else
					{
						RingBufferPut(&PlayingBuffer, (void*)pRxBlock);
					}
					__enable_irq();
					Tasks[DAC_TASK_NUMBER].ready = 1;
				}

				if (shouldBeTransmitted)
				{
					__disable_irq();
					if(RingBufferIsFull(&TxBuffer))
					{
						MemoryPoolReleaseBlock(pRxBlock->pPool, pRxBlock);
					}
					else
					{
						RingBufferPut(&TxBuffer, (void*)pRxBlock);
					}
					__enable_irq();
					Tasks[RIGHT_UART_TASK_NUMBER].ready = 1;
				}
			}
		}
		else
		{
				HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
			++InvalidCrc;
			__disable_irq();
			MemoryPoolReleaseBlock(pRxBlock->pPool, pRxBlock);
			__enable_irq();
		}

	}

	__disable_irq();
	pRxBlock = MemoryPoolGetBlock(&RxFramesMemoryPool);
	__enable_irq();

	if (NULL != pRxBlock)
	{
		__HAL_UART_ENABLE_IT(LeftUart.pUart, UART_IT_IDLE);
  	HAL_UART_Receive_DMA(LeftUart.pUart, (uint8_t*)pRxBlock->pData, sizeof(RsFrame));
  	Tasks[LEFT_UART_TASK_NUMBER].ready = 0;
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	__HAL_UART_DISABLE_IT(LeftUart.pUart, UART_IT_IDLE);
	__HAL_UART_CLEAR_IDLEFLAG(LeftUart.pUart);
	FrameReceived = 1;
	Tasks[LEFT_UART_TASK_NUMBER].ready = 1;
}

void HalUart2IdleInterruptHandler()
{
	// TODO consider AbortReceive and Abort ReceiveIT
	HAL_UART_Abort(LeftUart.pUart);
	HAL_UART_Receive_DMA(LeftUart.pUart, (uint8_t*)pRxBlock->pData, sizeof(RsFrame));
	__HAL_UART_CLEAR_IDLEFLAG(LeftUart.pUart);

//	HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
}

//void HAL_UART_AbortCpltCallback(UART_HandleTypeDef *huart)
//{
//	HAL_UART_Receive_DMA(LeftUart.pUart, (uint8_t*)pRxBlock->pData, sizeof(RsFrame));
//	__HAL_UART_ENABLE_IT(LeftUart.pUart, UART_IT_IDLE);  // Enable serial port idle interrupt
//}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  initUartHandles(&huart2, &huart1);

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM10_Init();
  MX_TIM11_Init();
  MX_USART1_UART_Init();
  MX_ADC_Init();
  MX_CRC_Init();
  MX_DAC_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  initUartPins();


  const uint8_t MemPoolSize = 16;
  if (MemoryPoolInit(&FramesMemoryPool, MemPoolSize, sizeof(RsFrame)))
  	Error_Handler();

  const uint8_t RxMemPoolSize = 16;
  if (MemoryPoolInit(&RxFramesMemoryPool, RxMemPoolSize, sizeof(RsFrame)))
  	Error_Handler();

  const uint8_t PlayingBufferSize = 16;
  if (RingBufferInit(&PlayingBuffer, PlayingBufferSize))
  	Error_Handler();

  const uint8_t TxBufferSize = 16;
  if (RingBufferInit(&TxBuffer, TxBufferSize))
  	Error_Handler();

  Tasks[ADC_TASK_NUMBER].pTask = AdcTask;
  Tasks[ADC_TASK_NUMBER].ready = 1;

  Tasks[DAC_TASK_NUMBER].pTask = DacTask;
  Tasks[DAC_TASK_NUMBER].ready = 0;

  Tasks[LEFT_UART_TASK_NUMBER].pTask = ReceiverTask;
  Tasks[LEFT_UART_TASK_NUMBER].ready = 1;

  Tasks[RIGHT_UART_TASK_NUMBER].pTask = TransmitterTask;
  Tasks[RIGHT_UART_TASK_NUMBER].ready = 0;

  /* USER CODE END 2 */
 
 

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  	for (volatile uint8_t i = 0; i < MAX_TASKS; ++i)
  	{
  		if (Tasks[i].pTask != NULL && Tasks[i].ready)
  		{
  			Tasks[i].pTask(TasksArguments[i]);
//  			break;
  		}
  	}
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLL_DIV3;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC_Init(void)
{

  /* USER CODE BEGIN ADC_Init 0 */

  /* USER CODE END ADC_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC_Init 1 */

  /* USER CODE END ADC_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  hadc.Init.LowPowerAutoWait = ADC_AUTOWAIT_DISABLE;
  hadc.Init.LowPowerAutoPowerOff = ADC_AUTOPOWEROFF_DISABLE;
  hadc.Init.ChannelsBank = ADC_CHANNELS_BANK_A;
  hadc.Init.ContinuousConvMode = DISABLE;
  hadc.Init.NbrOfConversion = 1;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T6_TRGO;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
  hadc.Init.DMAContinuousRequests = ENABLE;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_19;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_4CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC_Init 2 */

  /* USER CODE END ADC_Init 2 */

}

/**
  * @brief CRC Initialization Function
  * @param None
  * @retval None
  */
static void MX_CRC_Init(void)
{

  /* USER CODE BEGIN CRC_Init 0 */

  /* USER CODE END CRC_Init 0 */

  /* USER CODE BEGIN CRC_Init 1 */

  /* USER CODE END CRC_Init 1 */
  hcrc.Instance = CRC;
  if (HAL_CRC_Init(&hcrc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CRC_Init 2 */

  /* USER CODE END CRC_Init 2 */

}

/**
  * @brief DAC Initialization Function
  * @param None
  * @retval None
  */
static void MX_DAC_Init(void)
{

  /* USER CODE BEGIN DAC_Init 0 */

  /* USER CODE END DAC_Init 0 */

  DAC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN DAC_Init 1 */

  /* USER CODE END DAC_Init 1 */
  /** DAC Initialization 
  */
  hdac.Instance = DAC;
  if (HAL_DAC_Init(&hdac) != HAL_OK)
  {
    Error_Handler();
  }
  /** DAC channel OUT2 config 
  */
  sConfig.DAC_Trigger = DAC_TRIGGER_T7_TRGO;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  if (HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN DAC_Init 2 */

  /* USER CODE END DAC_Init 2 */

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 50-1;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 40-1;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief TIM7 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM7_Init(void)
{

  /* USER CODE BEGIN TIM7_Init 0 */

  /* USER CODE END TIM7_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM7_Init 1 */

  /* USER CODE END TIM7_Init 1 */
  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 50-1;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 40-1;
  htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM7_Init 2 */

  /* USER CODE END TIM7_Init 2 */

}

/**
  * @brief TIM10 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM10_Init(void)
{

  /* USER CODE BEGIN TIM10_Init 0 */

  /* USER CODE END TIM10_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};

  /* USER CODE BEGIN TIM10_Init 1 */

  /* USER CODE END TIM10_Init 1 */
  htim10.Instance = TIM10;
  htim10.Init.Prescaler = RECEIVER_TIMER_PRESCALER;
  htim10.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim10.Init.Period = RECEIVER_TIMER_PERIOD;
  htim10.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim10.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim10) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim10, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM10_Init 2 */

  /* USER CODE END TIM10_Init 2 */

}

/**
  * @brief TIM11 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM11_Init(void)
{

  /* USER CODE BEGIN TIM11_Init 0 */

  /* USER CODE END TIM11_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};

  /* USER CODE BEGIN TIM11_Init 1 */

  /* USER CODE END TIM11_Init 1 */
  htim11.Instance = TIM11;
  htim11.Init.Prescaler = RECEIVER_TIMER_PRESCALER;
  htim11.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim11.Init.Period = RECEIVER_TIMER_PERIOD;
  htim11.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim11.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim11) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim11, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM11_Init 2 */

  /* USER CODE END TIM11_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 1152000;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 1152000;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  /* DMA1_Channel3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel3_IRQn);
  /* DMA1_Channel4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);
  /* DMA1_Channel5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);
  /* DMA1_Channel6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);
  /* DMA1_Channel7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, USART3_TX_ENABLE_Pin|USART1_TX_ENABLE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(USART3_TX_ENABLEB2_GPIO_Port, USART3_TX_ENABLEB2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, LD4_Pin|LD3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : USART3_TX_ENABLE_Pin USART1_TX_ENABLE_Pin */
  GPIO_InitStruct.Pin = USART3_TX_ENABLE_Pin|USART1_TX_ENABLE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : USART3_TX_ENABLEB2_Pin */
  GPIO_InitStruct.Pin = USART3_TX_ENABLEB2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(USART3_TX_ENABLEB2_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LD4_Pin LD3_Pin */
  GPIO_InitStruct.Pin = LD4_Pin|LD3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */


/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM2 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM2) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
	HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, GPIO_PIN_SET);

	while(true)
	{
		HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
		HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin);
		HAL_Delay(500);
	}
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
