/*
 * Pin.c
 *
 *  Created on: Jan 6, 2021
 *      Author: dave
 */

#include "Pin.h"

void setPinState(const Pin p, GPIO_PinState state)
{
	HAL_GPIO_WritePin(p.port, p.pin, state);
}

void setPin(const Pin p)
{
	HAL_GPIO_WritePin(p.port, p.pin, GPIO_PIN_SET);
}

void resetPin(const Pin p)
{
	HAL_GPIO_WritePin(p.port, p.pin, GPIO_PIN_RESET);
}
